package com.atlassian.faila.scheduler

import org.junit.Before
import org.junit.Test
import org.mockito.Matchers.any
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import javax.jms.MessageProducer

class QuartzJobSchedulerTest {

    private val messageProducer: MessageProducer = mock(MessageProducer::class.java)

    private lateinit var jobScheduler: QuartzJobScheduler

    @Before
    fun setUp() {
        jobScheduler = QuartzJobScheduler(messageProducer)
    }

    @Test
    fun `job executed only once`() {
        jobScheduler.scheduleJob("0/1 * * * * ?", "anyInstanceId", setOf("anyAsg"))
        Thread.sleep(3000)
        verify(messageProducer, times(1)).send(any())
    }

}
