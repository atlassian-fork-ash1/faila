package com.atlassian.faila.cloud.aws

import com.amazonaws.AmazonClientException
import com.amazonaws.services.autoscaling.AmazonAutoScalingAsync
import com.amazonaws.services.autoscaling.model.AutoScalingGroup
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult
import com.amazonaws.services.autoscaling.model.Instance
import com.amazonaws.services.autoscaling.model.TagDescription
import com.amazonaws.services.ec2.AmazonEC2
import com.atlassian.faila.exceptions.AsgNotFoundException
import com.atlassian.faila.exceptions.EmptyAsgException
import com.atlassian.faila.exceptions.PerformFailaException
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.emptyCollectionOf
import org.hamcrest.Matchers.isIn
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.any
import org.mockito.Mockito.mock

class AWSCloudManagerTest {

    companion object {
        val ANY_ASGS = setOf("tools")
    }

    private lateinit var eC2Client: AmazonEC2
    private lateinit var awsCloudManager: AWSCloudManager
    private lateinit var autoScalingClient: AmazonAutoScalingAsync

    @Before
    fun setUp() {
        eC2Client = mock(AmazonEC2::class.java)
        autoScalingClient = mock(AmazonAutoScalingAsync::class.java)

        awsCloudManager = AWSCloudManager(autoScalingClient, eC2Client)
    }

    @Test(expected = PerformFailaException::class)
    fun `get random instance from asg throws perform exception on client failure`() {
        // arrange
        `when`(autoScalingClient.describeAutoScalingGroups(any())).thenThrow(AmazonClientException::class.java)

        // act
        awsCloudManager.getRandomInstanceFromAsgs(ANY_ASGS)

        // assert
    }

    @Test(expected = EmptyAsgException::class)
    fun `get random instance from asg throws empty asg exception`() {
        // arrange
        val asg: AutoScalingGroup = mock(AutoScalingGroup::class.java)
        val clientResult = mock(DescribeAutoScalingGroupsResult::class.java)
        `when`(autoScalingClient.describeAutoScalingGroups(any())).thenReturn(clientResult)
        `when`(clientResult.autoScalingGroups).thenReturn(listOf(asg))
        `when`(asg.instances).thenReturn(emptyList())

        // act
        awsCloudManager.getRandomInstanceFromAsgs(ANY_ASGS)

        // assert
    }

    @Test(expected = AsgNotFoundException::class)
    fun `get random instance from asg throws asg not found`() {
        // arrange
        val clientResult = mock(DescribeAutoScalingGroupsResult::class.java)
        `when`(autoScalingClient.describeAutoScalingGroups(any())).thenReturn(clientResult)
        `when`(clientResult.autoScalingGroups).thenReturn(emptyList())

        // act
        awsCloudManager.getRandomInstanceFromAsgs(ANY_ASGS)

        // assert
    }

    @Test
    fun `get random instance from asg returns instance`() {
        // arrange
        val asg: AutoScalingGroup = mock(AutoScalingGroup::class.java)
        val clientResult = mock(DescribeAutoScalingGroupsResult::class.java)
        `when`(autoScalingClient.describeAutoScalingGroups(any())).thenReturn(clientResult)
        `when`(clientResult.autoScalingGroups).thenReturn(listOf(asg))
        val instance1: Instance = mock(Instance::class.java)
        val instance2: Instance = mock(Instance::class.java)
        `when`(instance1.instanceId).thenReturn("1")
        `when`(instance2.instanceId).thenReturn("2")
        `when`(asg.instances).thenReturn(listOf(instance1, instance2))

        // act
        val result = awsCloudManager.getRandomInstanceFromAsgs(ANY_ASGS)

        // assert
        assertThat(result, isIn(listOf("1", "2")))
    }

    @Test(expected = PerformFailaException::class)
    fun `terminate instance throws perform exception on client failure`() {
        // arrange
        `when`(eC2Client.terminateInstances(any())).thenThrow(AmazonClientException::class.java)

        // act
        awsCloudManager.terminateInstance("any_id")

        // assert
    }

    @Test
    fun `get tagged asg ids return empty list on client failure`() {
        // arrange
        `when`(autoScalingClient.describeAutoScalingGroups(any())).thenThrow(AmazonClientException::class.java)

        // act
        val result = awsCloudManager.getTaggedAsgIds()

        // assert
        assertThat(result, emptyCollectionOf(String::class.java))
    }

    @Test
    fun `get tagged asg ids return empty list when no asg has faila tag`() {
        // arrange
        val asg: AutoScalingGroup = mock(AutoScalingGroup::class.java)
        val clientResult = mock(DescribeAutoScalingGroupsResult::class.java)
        `when`(autoScalingClient.describeAutoScalingGroups(any())).thenReturn(clientResult)
        `when`(clientResult.autoScalingGroups).thenReturn(listOf(asg))
        `when`(asg.tags).thenReturn(mutableListOf(
            TagDescription().withKey("something").withValue("something")))

        // act
        val result = awsCloudManager.getTaggedAsgIds()

        // assert
        assertThat(result, emptyCollectionOf(String::class.java))
    }

    @Test
    fun `get tagged asg ids return asgs with faila tag`() {
        // arrange
        val asg: AutoScalingGroup = mock(AutoScalingGroup::class.java)
        val clientResult = mock(DescribeAutoScalingGroupsResult::class.java)
        `when`(autoScalingClient.describeAutoScalingGroups(any())).thenReturn(clientResult)
        `when`(clientResult.autoScalingGroups).thenReturn(listOf(asg))
        `when`(asg.tags).thenReturn(mutableListOf(
            TagDescription().withKey("faila:termination").withValue("something")))
        `when`(asg.autoScalingGroupName).thenReturn("expectedName")

        // act
        val result = awsCloudManager.getTaggedAsgIds()

        // assert
        assertThat(result, `is`(listOf("expectedName")))
    }
}
