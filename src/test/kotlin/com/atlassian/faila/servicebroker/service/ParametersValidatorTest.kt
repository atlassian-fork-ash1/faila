package com.atlassian.faila.servicebroker.service

import com.atlassian.faila.model.Faila
import org.junit.Test
import org.springframework.cloud.servicebroker.exception.ServiceBrokerInvalidParametersException

class ParametersValidatorTest {
    private val validator = ParametersValidator()
    private val validSchedule = "0 0 12 * * ?"
    private val validType = "termination"
    private val asgs = listOf("asgs")
    private val dryRun = true

    @Test(expected = ServiceBrokerInvalidParametersException::class)
    fun `null parameters not allowed`() {
        validator.validate(null)
    }

    @Test(expected = ServiceBrokerInvalidParametersException::class)
    fun `invalid type not allowed`() {
        val parameters = hashMapOf(
                "type" to "hydration",
                "schedule" to validSchedule,
                "asgs" to asgs,
                "dryRun" to dryRun)
        validator.validate(parameters)
    }

    @Test(expected = ServiceBrokerInvalidParametersException::class)
    fun `invalid schedule not allowed`() {
        val parameters = hashMapOf(
                "type" to validType,
                "schedule" to "0 0 12 * *",
                "asgs" to asgs,
                "dryRun" to dryRun
        )
        validator.validate(parameters)
    }

    @Test
    fun `valid parameters do not throw exceptions`() {
        val parameters = hashMapOf(
                "type" to validType,
                "schedule" to validSchedule,
                "asgs" to asgs,
                "dryRun" to dryRun
        )
        validator.validate(parameters)
    }

    @Test(expected = ServiceBrokerInvalidParametersException::class)
    fun `non-zero priority requires schedule`() {
        val parameters = hashMapOf(
            "asgs" to asgs.toList(),
            "probability" to 1,
            "type" to Faila.DEFAULT_TYPE
        )
        validator.validate(parameters)
    }

    @Test(expected = ServiceBrokerInvalidParametersException::class)
    fun `non-zero priority requires type`() {
        val parameters = hashMapOf(
            "asgs" to asgs.toList(),
            "probability" to 1,
            "schedule" to Faila.DEFAULT_SCHEDULE
        )
        validator.validate(parameters)
    }

    @Test
    fun `zero priority means fewer required fields`() {
        val parameters = hashMapOf(
                "asgs" to asgs.toList(),
                "probability" to 0
        )
        validator.validate(parameters)
    }

}
