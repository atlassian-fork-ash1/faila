package com.atlassian.faila.servicebroker.instances.dynamo

import com.atlassian.faila.model.Faila
import org.socialsignin.spring.data.dynamodb.repository.EnableScan
import org.springframework.data.repository.CrudRepository

@EnableScan
interface FailaDAO : CrudRepository<Faila, String>
