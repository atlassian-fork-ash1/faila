package com.atlassian.faila.servicebroker.instances.tags

import com.atlassian.faila.cloud.CloudManager
import com.atlassian.faila.exceptions.OutdatedScheduledFailaException
import com.atlassian.faila.model.Faila
import com.atlassian.faila.servicebroker.instances.ServiceInstanceManager
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.servicebroker.exception.ServiceInstanceDoesNotExistException

class TagsServiceInstanceManager @Autowired constructor(private val cloudManager: CloudManager) :
    ServiceInstanceManager() {

    companion object {
        private val LOG = LoggerFactory.getLogger(TagsServiceInstanceManager::class.java)
    }

    override fun createFaila(serviceInstanceId: String, parameters: Map<String, Any>): Faila {
        LOG.info("Creating new faila '$serviceInstanceId', parameters: $parameters")
        val result: Faila = failaFromMap(serviceInstanceId, parameters)
        cloudManager.putTag(result.asgs, tagFromFaila(result))
        return result
    }

    override fun deleteFaila(serviceInstanceId: String) {
        LOG.info("Deleting faila '$serviceInstanceId'")
        val existingFaila: Faila = findFaila(serviceInstanceId) ?: return
        cloudManager.deleteTag(existingFaila.asgs, tagFromFaila(existingFaila).first)
    }

    override fun updateFaila(serviceInstanceId: String, parameters: Map<String, Any>): Faila {
        val existingFaila: Faila = findFaila(serviceInstanceId) ?: run {
            LOG.error("Couldn't update service instance '$serviceInstanceId', it doesn't exist")
            throw ServiceInstanceDoesNotExistException(serviceInstanceId)
        }

        val updatedFaila = failaFromMap(serviceInstanceId, parameters)
        if (existingFaila == updatedFaila) {
            return existingFaila
        }

        cloudManager.putTag(updatedFaila.asgs, tagFromFaila(updatedFaila))
        return updatedFaila
    }

    override fun listFailas(): Iterable<Faila> {
        val taggedAsgIds: List<String> = cloudManager.getTaggedAsgIds()
        val failaMap: MutableMap<Pair<String, String>, MutableSet<String>> = mutableMapOf()
        taggedAsgIds.forEach { asgId ->
            cloudManager.getFailaTag(asgId)?.let {
                failaMap.getOrPut(it, { mutableSetOf() }).add(asgId)
            }
        }
        return failaMap.map {
            failaFromTag(it.value, it.key.first, it.key.second)
        }
    }

    override fun findFailaAndResetNextFire(serviceInstanceId: String, nextFire: String, asgs: Set<String>): Faila {
        failaFromAsgs(asgs)?.let {
            if (it.nextFire == nextFire && it.id == serviceInstanceId) {
                it.nextFire = ""
                cloudManager.putTag(it.asgs, tagFromFaila(it))
                return it
            }
        }

        throw OutdatedScheduledFailaException()
    }

    override fun setFailaNextFire(serviceInstanceId: String, nextFire: String) {
        val existingFaila = findFaila(serviceInstanceId) ?: return
        existingFaila.nextFire = nextFire
        cloudManager.putTag(existingFaila.asgs, tagFromFaila(existingFaila))
    }

    override fun findFaila(serviceInstanceId: String): Faila? {
        return listFailas().firstOrNull { it.id == serviceInstanceId }
    }

    private fun failaFromAsgs(asgs: Set<String>): Faila? {
        val failaTag: Pair<String, String> = cloudManager.getFailaTag(asgs) ?: return null
        return failaFromTag(asgs, failaTag.first, failaTag.second)
    }

    private fun failaFromTag(asgs: Set<String>, key: String, value: String): Faila = Faila().apply {
        this.asgs = asgs
        type = key.replace("faila:", "")
        val parameters: List<String> = value.split(":")
        id = parameters[0]
        schedule = parameters[1]
        dryRun = parameters[2].toBoolean()
        probability = parameters[3].toInt()
        nextFire = parameters[4]
    }

    private fun tagFromFaila(faila: Faila): Pair<String, String> {
        val key = "faila:${faila.type}"
        val value = "${faila.id}:${faila.schedule}:${faila.dryRun}:${faila.probability}:${faila.nextFire}"
        return Pair(key, value)
    }

}
