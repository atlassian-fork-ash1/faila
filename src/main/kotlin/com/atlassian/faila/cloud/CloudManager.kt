package com.atlassian.faila.cloud

interface CloudManager {

    fun getRandomInstanceFromAsgs(asgs: Set<String>): String

    fun terminateInstance(instanceId: String)

    fun getTaggedAsgIds(): List<String>

    fun getFailaTag(asgs: Set<String>): Pair<String, String>?

    fun getFailaTag(asg: String): Pair<String, String>?

    fun putTag(asgs: Set<String>, tag: Pair<String, String>)

    fun deleteTag(asgs: Set<String>, key: String)

}
